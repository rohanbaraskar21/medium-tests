import io.restassured.RestAssured.given
import org.junit.Test

class MediumTests {

    @Test
    fun firstTest() {
        given()
        .`when`()
            .get("https://medium.com/@artemmezdrin")
        .then()
            .statusCode(200)
            .log().body()
    }

    @Test
    fun testShouldBeFailedButWasFixed() {
        given()
        .`when`()
            .get("https://medium.com/com.medium//https")
        .then()
            .statusCode(404)
    }
}